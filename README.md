# README #

### What is this repository for? ###

* This project aims to provide archaeologists with a mobile system that can reconstruct an underground environment, with real-time partial or full feedback.
* [Wiki Page](https://bitbucket.org/cse145237d3dreconstruction/3d-reconstruction/wiki/Home)

### How do I get set up? ###
* [Set up for Tango using Unity](https://bitbucket.org/cse145237d3dreconstruction/3d-reconstruction/wiki/Set%20Up%20Instructions%20(Project%20Tango%20Unity)) 
* [Setup for Kinect](https://bitbucket.org/cse145237d3dreconstruction/3d-reconstruction/wiki/Set%20up%20for%20Kinect)

### Who do I talk to? ###

* [Contact Info](https://bitbucket.org/cse145237d3dreconstruction/3d-reconstruction/wiki/Team%20Members)